# Generated from Expr.g4 by ANTLR 4.5.1
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2")
        buf.write(u"\25j\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write(u"\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t")
        buf.write(u"\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write(u"\4\23\t\23\4\24\t\24\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\4")
        buf.write(u"\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b")
        buf.write(u"\3\b\3\t\3\t\7\tB\n\t\f\t\16\tE\13\t\3\n\6\nH\n\n\r\n")
        buf.write(u"\16\nI\3\13\6\13M\n\13\r\13\16\13N\3\f\3\f\3\r\3\r\3")
        buf.write(u"\16\3\16\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\21\3\21")
        buf.write(u"\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\24\3\24\3")
        buf.write(u"\24\2\2\25\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25")
        buf.write(u"\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25\3\2")
        buf.write(u"\6\6\2&&C\\aac|\7\2&&\62;C\\aac|\4\2\f\f\17\17\3\2\62")
        buf.write(u";l\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2")
        buf.write(u"\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23")
        buf.write(u"\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33")
        buf.write(u"\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2")
        buf.write(u"\2\2\2%\3\2\2\2\2\'\3\2\2\2\3)\3\2\2\2\5+\3\2\2\2\7.")
        buf.write(u"\3\2\2\2\t\61\3\2\2\2\13\64\3\2\2\2\r\66\3\2\2\2\178")
        buf.write(u"\3\2\2\2\21?\3\2\2\2\23G\3\2\2\2\25L\3\2\2\2\27P\3\2")
        buf.write(u"\2\2\31R\3\2\2\2\33T\3\2\2\2\35V\3\2\2\2\37X\3\2\2\2")
        buf.write(u"!]\3\2\2\2#a\3\2\2\2%c\3\2\2\2\'e\3\2\2\2)*\7?\2\2*\4")
        buf.write(u"\3\2\2\2+,\7\"\2\2,-\7*\2\2-\6\3\2\2\2./\7+\2\2/\60\7")
        buf.write(u"\"\2\2\60\b\3\2\2\2\61\62\7.\2\2\62\63\7\"\2\2\63\n\3")
        buf.write(u"\2\2\2\64\65\7*\2\2\65\f\3\2\2\2\66\67\7+\2\2\67\16\3")
        buf.write(u"\2\2\289\7r\2\29:\7t\2\2:;\7k\2\2;<\7p\2\2<=\7v\2\2=")
        buf.write(u">\7*\2\2>\20\3\2\2\2?C\t\2\2\2@B\t\3\2\2A@\3\2\2\2BE")
        buf.write(u"\3\2\2\2CA\3\2\2\2CD\3\2\2\2D\22\3\2\2\2EC\3\2\2\2FH")
        buf.write(u"\t\4\2\2GF\3\2\2\2HI\3\2\2\2IG\3\2\2\2IJ\3\2\2\2J\24")
        buf.write(u"\3\2\2\2KM\t\5\2\2LK\3\2\2\2MN\3\2\2\2NL\3\2\2\2NO\3")
        buf.write(u"\2\2\2O\26\3\2\2\2PQ\7-\2\2Q\30\3\2\2\2RS\7/\2\2S\32")
        buf.write(u"\3\2\2\2TU\7,\2\2U\34\3\2\2\2VW\7\61\2\2W\36\3\2\2\2")
        buf.write(u"XY\7\"\2\2YZ\7\"\2\2Z[\7\"\2\2[\\\7\"\2\2\\ \3\2\2\2")
        buf.write(u"]^\7h\2\2^_\7p\2\2_`\7\"\2\2`\"\3\2\2\2ab\7]\2\2b$\3")
        buf.write(u"\2\2\2cd\7_\2\2d&\3\2\2\2ef\7t\2\2fg\7g\2\2gh\7v\2\2")
        buf.write(u"hi\7\"\2\2i(\3\2\2\2\6\2CIN\2")
        return buf.getvalue()


class ExprLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]


    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    ID = 8
    NEWLINE = 9
    INT = 10
    PLUS = 11
    MINUS = 12
    TIMES = 13
    DIVIDE = 14
    TAB = 15
    FN = 16
    OPEN_SCOPE = 17
    CLOSE_SCOPE = 18
    RET = 19

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'='", u"' ('", u"') '", u"', '", u"'('", u"')'", u"'print('", 
            u"'+'", u"'-'", u"'*'", u"'/'", u"'    '", u"'fn '", u"'['", 
            u"']'", u"'ret '" ]

    symbolicNames = [ u"<INVALID>",
            u"ID", u"NEWLINE", u"INT", u"PLUS", u"MINUS", u"TIMES", u"DIVIDE", 
            u"TAB", u"FN", u"OPEN_SCOPE", u"CLOSE_SCOPE", u"RET" ]

    ruleNames = [ u"T__0", u"T__1", u"T__2", u"T__3", u"T__4", u"T__5", 
                  u"T__6", u"ID", u"NEWLINE", u"INT", u"PLUS", u"MINUS", 
                  u"TIMES", u"DIVIDE", u"TAB", u"FN", u"OPEN_SCOPE", u"CLOSE_SCOPE", 
                  u"RET" ]

    grammarFileName = u"Expr.g4"

    def __init__(self, input=None):
        super(ExprLexer, self).__init__(input)
        self.checkVersion("4.5.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


